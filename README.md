# README #

### Marblelanding ###

* **marblelanding** - это django wagtail сайт.
* 0.0.0

### Настройка и запуск ###

1. создай окружение для решения находясь внутри директории в которою вы выгрузили проект:

     `virtualenv marblelanding`

2. перейди в корневую папку окружения:

     `cd marblelanding`

3. активируй окружение:

    `source bin/activate`

4. перейди в директорию app:

     `cd app`

5. установи python пакеты:
     `pip3 install -r requirements.txt`

6. настрой ноду внутри окружения.Это построит все необходимые для node зависимости:

     `nodeenv -p A`

7. ставь node пакеты:

     `npm install`

8. запускай django строить миграции:

     `python3 manage.py makemigrations`

9. строй sqlight базу данных:

     `python3 manage.py migrate`

10. создавай superuser'а:

     `python3 manage.py createsuperuser`

11. настраивай superuser'а в терменале

12. запускай сервер:

     `python3 manage.py runserver`

13. переходи на локальный [8000 порт](http://127.0.0.1:8000/)

14. чтобы завершить работу сервера и освободить 8000 порт жми сочитание клавишь `Ctr+C`

### Перезапуск ###

В связи с тем, что тащить на сервер все миграции в DB - это как минимум не является необходимостью,а как максимум расходует трафик на сервере, отныне и впредь миграции мы будем создавать на локальных машинах.

Каждый раз когда делаешь pull или fork с сервера, в папке с manage.py

1. запускай django строить миграции:

     `python3 manage.py makemigrations`

2. строй sqlight базу данных:

     `python3 manage.py migrate`

3. запускай сервер:

     `python3 manage.py runserver`