from django.db import models

from wagtail.wagtailcore import blocks

from modelcluster.fields import ParentalKey

from wagtail.wagtailadmin.edit_handlers import FieldPanel, FieldRowPanel, InlinePanel, MultiFieldPanel, StreamFieldPanel

from wagtail.wagtailforms.edit_handlers import FormSubmissionsPanel

from wagtail.wagtailadmin.utils import send_mail

from wagtail.wagtailimages.blocks import ImageChooserBlock
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel

from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailforms.models import AbstractFormField, AbstractEmailForm

from wagtail.contrib.table_block.blocks import TableBlock as BaseTableBlock

from .filters import *

class PresentationBlock(blocks.StructBlock):
	title = blocks.CharBlock(label='Название')
	image = ImageChooserBlock(label='Изображение')
	description = blocks.RichTextBlock(label='Описание')

	class Meta:
		template='blocks/presentation_block.html'

class InfographicBlock(blocks.StructBlock):
	image = ImageChooserBlock(label='Изображение')
	description = blocks.CharBlock(label='Описание')

	class Meta:
		template='blocks/infographic_block.html'

class ReferenceBlock(blocks.StructBlock):
	image = ImageChooserBlock(label='Изображение')
	name = blocks.CharBlock(label='Описание')
	url = blocks.URLBlock(label='Ссылка')

	class Meta:
		template='blocks/references_block.html'
	
class LandingBlock(blocks.StructBlock):
	title = blocks.CharBlock(label='Заголовок', required=True, unique=True)
	show_title = blocks.BooleanBlock(label='Показывать заголовок', default=True, required=False)
	subtitle = blocks.RichTextBlock(label='Подзаголовок', required=False)
	background = ImageChooserBlock(label='Задний фон', required=False)
	animate_background = blocks.BooleanBlock(label='Анимированный фон', default=True, required=False)

class CarouselBlock(LandingBlock):
	content = blocks.StreamBlock([
		('contenttable', BaseTableBlock(label='Таблица', template='blocks/base_table_block.html')),
		('image', ImageChooserBlock(label='Изображение')),
		('text', blocks.RichTextBlock(label='Текст')),
	], label='Содержимое')

	class Meta:
		template='blocks/carousel_block.html'

class TextBlock(LandingBlock):
	content = blocks.RichTextBlock(label='Содержимое', required=False)

	class Meta:
		template='blocks/text_block.html'

class TableBlock(LandingBlock):
	content = BaseTableBlock(label='Содержимое', template='blocks/base_table_block.html')

	class Meta:
		template='blocks/table_block.html'

class GroupBlock(LandingBlock):
	TWO = 'col-md-6 col-sm-6 col-xs-12'
	THREE = 'col-md-4 col-sm-6 col-xs-12'
	FOUR = 'col-md-3 col-sm-6 col-xs-12'
	FIVE = 'col-md-2 col-sm-6 col-xs-12'
	COUNT_OPTIONS = (
		(TWO, 'Два'),
		( THREE, 'Три'),
		(FOUR, 'Четыре'),
		(FIVE, 'Пять')
	)
	count_class = blocks.ChoiceBlock(label='Элементов в строке', choices=COUNT_OPTIONS, default=THREE)

	class Meta:
		template='blocks/group_block.html'

class FormField(AbstractFormField):
	page = ParentalKey('LandingPage', related_name='form_fields')

class LandingPage(AbstractEmailForm):
	subtitle = models.CharField(max_length=250, verbose_name='Подзаголовок', blank=True, null='True')
	logo = models.ForeignKey('wagtailimages.Image', blank=True, null='True', on_delete=models.SET_NULL)

	body = StreamField(
		[
			('richtext', TextBlock(label='Текст')),
			('contenttable', TableBlock(label='Таблица')),
			('presentation', GroupBlock(
				[
					('elements', blocks.ListBlock(PresentationBlock(), label='Компоненты')),
				], label='Презентация'
			)),
			('infographic', GroupBlock(
				[
					('elements', blocks.ListBlock(InfographicBlock(), label='Компоненты')),
				], label='Инфографика'
			)),
			('references', GroupBlock(
				[
					('elements', blocks.ListBlock(ReferenceBlock(), label='Компоненты')),
				], label='Отсылки'
			)),
			('carousel', CarouselBlock(label='Карусель')),
		], verbose_name='Содержимое', blank=True
	)

	content_panels = [
		MultiFieldPanel([
			FieldPanel('title'),
			FieldPanel('subtitle'),
			ImageChooserPanel('logo')
		], 'Заглавный блок'),
		StreamFieldPanel('body'),
		InlinePanel('form_fields', label='Поля формы'),
		MultiFieldPanel([
			FieldRowPanel([
				FieldPanel('from_address', classname='col6'),
				FieldPanel('to_address', classname='col6'),
			]),
			FieldPanel('subject'),
		], 'Рассылка')
	]

	parent_page_types = [
		'wagtailcore.Page'
	]

	@classmethod
	def can_create_at(cls, parent):
		return super(LandingPage, cls).can_create_at(parent) and not cls.objects.exists()

	class Meta:
		verbose_name = 'Продающая страница'
		verbose_name_plural = 'Продающая страницу'
