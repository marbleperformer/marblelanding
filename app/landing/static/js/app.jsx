$('.block-section').each((idx, itm)=>{
	var linkTo = '#' + $(itm).attr('id')
	var linkName = $(itm).attr('name')
	var list_item = $('<li>').appendTo('.navbar-hash .nav')
	$('<a/>', { 
		href:linkTo, 
		text:linkName, 
		click: (event)=>{
			$('html, body').animate({
				scrollTop: $(linkTo).offset().top
			}, 1000)
		}
	}).appendTo(list_item)
})
