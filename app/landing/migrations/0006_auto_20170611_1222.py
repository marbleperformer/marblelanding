# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-11 05:22
from __future__ import unicode_literals

from django.db import migrations
import wagtail.contrib.table_block.blocks
import wagtail.wagtailcore.blocks
import wagtail.wagtailcore.fields
import wagtail.wagtailimages.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0005_auto_20170608_2303'),
    ]

    operations = [
        migrations.AlterField(
            model_name='landingpage',
            name='body',
            field=wagtail.wagtailcore.fields.StreamField((('Текст', wagtail.wagtailcore.blocks.RichTextBlock()), ('Таблица', wagtail.contrib.table_block.blocks.TableBlock()), ('Презентация', wagtail.wagtailcore.blocks.StructBlock((('title', wagtail.wagtailcore.blocks.CharBlock(label='Заголовок')), ('subtitle', wagtail.wagtailcore.blocks.RichTextBlock(label='Подзаголовок')), ('background', wagtail.wagtailimages.blocks.ImageChooserBlock(label='Задний фон')), ('Компоненты', wagtail.wagtailcore.blocks.ListBlock(wagtail.wagtailcore.blocks.StructBlock((('title', wagtail.wagtailcore.blocks.CharBlock(label='Подзаголовок')), ('image', wagtail.wagtailimages.blocks.ImageChooserBlock(label='Изображение')), ('description', wagtail.wagtailcore.blocks.RichTextBlock(label='Описание'))))))))), ('Инфографика', wagtail.wagtailcore.blocks.StructBlock((('title', wagtail.wagtailcore.blocks.CharBlock(label='Заголовок')), ('subtitle', wagtail.wagtailcore.blocks.RichTextBlock(label='Подзаголовок')), ('background', wagtail.wagtailimages.blocks.ImageChooserBlock(label='Задний фон')), ('Компоненты', wagtail.wagtailcore.blocks.ListBlock(wagtail.wagtailcore.blocks.StructBlock((('image', wagtail.wagtailimages.blocks.ImageChooserBlock(label='Изображение')), ('description', wagtail.wagtailcore.blocks.CharBlock(label='Описание'))))))))), ('Партнеры', wagtail.wagtailcore.blocks.StructBlock((('title', wagtail.wagtailcore.blocks.CharBlock(label='Заголовок')), ('subtitle', wagtail.wagtailcore.blocks.RichTextBlock(label='Подзаголовок')), ('background', wagtail.wagtailimages.blocks.ImageChooserBlock(label='Задний фон')), ('Компоненты', wagtail.wagtailcore.blocks.ListBlock(wagtail.wagtailcore.blocks.StructBlock((('image', wagtail.wagtailimages.blocks.ImageChooserBlock(label='Изображение')), ('name', wagtail.wagtailcore.blocks.RichTextBlock(label='Описание')), ('url', wagtail.wagtailcore.blocks.URLBlock(label='Ссылка'))))))))), ('Карусель', wagtail.wagtailcore.blocks.StructBlock((('title', wagtail.wagtailcore.blocks.CharBlock(label='Заголовок')), ('subtitle', wagtail.wagtailcore.blocks.RichTextBlock(label='Подзаголовок')), ('background', wagtail.wagtailimages.blocks.ImageChooserBlock(label='Задний фон')), ('Компоненты', wagtail.wagtailcore.blocks.StructBlock((('body', wagtail.wagtailcore.blocks.StreamBlock((('Таблица', wagtail.contrib.table_block.blocks.TableBlock()), ('Партнеры', wagtail.wagtailcore.blocks.StructBlock((('image', wagtail.wagtailimages.blocks.ImageChooserBlock(label='Изображение')), ('name', wagtail.wagtailcore.blocks.RichTextBlock(label='Описание')), ('url', wagtail.wagtailcore.blocks.URLBlock(label='Ссылка')))))))),))))))), blank=True, verbose_name='Содержание'),
        ),
    ]
