var path = require('path')

var webpack = require('webpack')

const NODE_ENV = process.env.NODE_ENV || 'dev'

var bootastrap_js_path = path.resolve(__dirname, 'node_modules', 'bootstrap-sass/assets/javascripts/bootstrap.js')

var bootastrap_css_path = path.resolve(__dirname, 'node_modules', 'bootstrap-sass/assets/stylesheets/_bootstrap.scss')

var general_css_path = path.resolve(__dirname, 'app', 'static/css/general.scss')

module.exports = {
	entry: {
		base: './app/static/index.js',
		brief: './brief/static/index.js',
		landing: './landing/static/index.js',
		vendor: ['jquery', 'jquery-ui', bootastrap_js_path, bootastrap_css_path, general_css_path]
	},
	resolve: {
		modules: ['node_modules', 'bower_components']
	},
	watch: NODE_ENV == 'dev',
	output: {
		path: __dirname + '/app/static/build',
		filename: '[name].bundle.js'
	},
	module: {
		loaders: [
			{
				test: /\.jsx$/,
				loader: 'babel-loader?optional[]=runtime',
				include: [
					path.resolve(__dirname, 'node_modules')
				],
				query: {
					presets: ['es2015', 'react']
				}
			},
			{
				test: /\.s[a|c]ss$/,
				loader:'style-loader!css-loader!sass-loader'
			},
			{
				test: /\.css$/,
				loader:'style-loader!css-loader!sass-loader'
			},
			{ 
				test: /\.(ttf|eot)$/, 
				loader: 'file-loader?name=[name].[ext]' 
			},
			{ 
				test: /\.(woff2?|svg|png)$/, 
				loader: 'url-loader?limit=10000&name=[name].[ext]' 
			}
		]
	},
	plugins: [
		new webpack.optimize.CommonsChunkPlugin(
			{
				name:'vendor', 
				filename:'vendor.bundle.js'
			}
		),
		new webpack.ProvidePlugin({
			$:'jquery',
			jQuery:'jquery'
		})
	]
};
